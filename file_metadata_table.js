(function ($, Drupal) {

  "use strict";

  var showText = Drupal.t('Show hidden files');
  var hideText = Drupal.t('Hide hidden files');
  var toggleState = false;

  function hideRows ($table) {
    var tableDragObject = Drupal.tableDrag[$table.attr('id')];
    var $toggleLink = $('<a href="#" class="file-metadata-table-toggle-row"></a>');
    $toggleLink
      .attr('title', Drupal.t('Show or hide rows with hidden files.'))
      .bind('click.metadataTable', function (e) {
        $table.find('.file-display:not(:checked)').closest('tr').toggle(toggleState);
        tableDragObject.restripeTable();
        $toggleLink.html(toggleState ? hideText : showText);
        toggleState = !toggleState;
        e.preventDefault();
      });
    // Add a toggle after the table to allow users to show or hide hidden files.
    $table.after(
      $('<div class="file-metadata-table-toggle-row-wrapper more-link" />').prepend($toggleLink)
    );
  }

  Drupal.behaviors.fileMetadataTable = {
    attach: function (context) {
      var $table = $(context).find('.field-widget-file-generic table[id]').once('file-metadata-advanced');
      if ($table.length) {
        hideRows($table);
      }
      toggleState = false;
      // By default hide the rows for hidden files.
      $('.file-metadata-table-toggle-row').click();
    },
    detach: function (context, settings, trigger) {
      if (trigger === 'unload') {
        var $table = $(context).find('.field-widget-file-generic table[id]').removeOnce('file-metadata-advanced');
        if ($table.length) {
          // Force display of hidden rows.
          toggleState = true;
          // We need to select the parent because the link is outside the table.
          $table.parent().find('.file-metadata-table-toggle-row').click()
            .unbind('click.metadataTable').remove();
        }
      }
    }
  };

})(jQuery, Drupal);
